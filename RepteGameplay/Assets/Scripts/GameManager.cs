using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public int vidas = 1;

    private void Awake()
    {
        if ( Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Hay mas de un Game Manager en la escena");
        }
    }
    void Start()
    {

    }

    // Ya no utilizo esto
    public int Morir()
    {
        vidas -= 1;

        if (vidas == 0)
        {
            //SceneManager.LoadScene("ResultScreen");
        }
        return vidas;
    }
}
