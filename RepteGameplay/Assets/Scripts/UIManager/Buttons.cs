using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
	private GameObject _txt;
	private InputField _inputField;
	private string _name;
	// Start is called before the first frame update
	public void Game1Button()
	{
		_txt = GameObject.Find("Name");
		_inputField = _txt.GetComponent<InputField>();
		_name = _inputField.text;
		if (_name != "")
		{
			PlayerPrefs.SetString("nom", _name);
			PlayerPrefs.SetInt("scene", 1);
			SceneManager.LoadScene("GameScene");
		}
	}
	public void Game2Button()
	{
		_txt = GameObject.Find("Name");
		_inputField = _txt.GetComponent<InputField>();
		_name = _inputField.text;
		if (_name != "")
		{
			PlayerPrefs.SetString("nom", _name);
			PlayerPrefs.SetInt("scene", 1);
			SceneManager.LoadScene("GameScene2");
		}
		
	}
	public void MenuButton()
	{
		SceneManager.LoadScene("StartScreen");
	}
	
}
