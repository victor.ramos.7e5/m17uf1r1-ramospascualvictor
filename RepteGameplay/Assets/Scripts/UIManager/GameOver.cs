using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public TimeController timecontrol;
   public void Setup()
    {
        gameObject.SetActive(true);
        timecontrol.Desactivar();
    }
	public void Game1Button()
	{
		SceneManager.LoadScene("GameScene");
	}
	public void Game2Button()
	{
		SceneManager.LoadScene("GameScene2");
	}
	public void MenuButton()
	{
		SceneManager.LoadScene("StartScreen");
	}

}
