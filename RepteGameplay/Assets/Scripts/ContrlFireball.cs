using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContrlFireball : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;
    
    public float couldownFireballs = 1f;
    private float cooldownFire = 0;

    [SerializeField]
    private GameObject _fireball;

    private void Start()
    {

    }


    void Update()
    {
        SpawnFireballs();
    }

    void SpawnFireballs()
    {
        if (cooldownFire <= 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var click = _camera.ScreenToWorldPoint(Input.mousePosition);
                click[1] = 10;
                click[2] = 0;

                Instantiate(_fireball, click, new Quaternion(0f, 0f, 0f, 0f));
                cooldownFire = couldownFireballs;
            }
        }
        else cooldownFire -= Time.deltaTime;
    }

}
