using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoMov : MonoBehaviour
{
    private SpriteRenderer _sprite;
    private Animator _animator;
    private RaycastHit2D _hitGround;
    private RaycastHit2D _hitFire;

    private bool _raycast = false;
    private float _speed = 0.02f;
    private int _direction = 1;
    public GameOver gameover;
    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        MoveAuto();
        _hitGround = Physics2D.Raycast(this.transform.position, new Vector3(_direction, -1, 0));
        _hitFire = Physics2D.Raycast(this.transform.position, new Vector3(_direction, 1, 0));
        

        if (!_hitGround) _raycast = true;
        else if (_hitGround.collider.tag == "Ground") _raycast = true;

        if (_hitFire.collider.tag == "FireBall") _raycast = true;
        if (transform.position.y <= -3 || transform.position.y >= 10)
        {
            gameover.Setup();
            //SceneManager.LoadScene("ResultScreen");
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("FireBall"))
        {

            // FadeOut();
            SceneManager.LoadScene("ResultScreen");
        }


    }

    private void MoveAuto()
    {

        if (_raycast)
        {
            _direction *= -1;
            _speed *= -1;
            _sprite.flipX = (_sprite.flipX) ? false : true;
        }

        transform.position += new Vector3(_speed, 0, 0);
        _animator.SetBool("isRunning", _speed != 0.0f);
    }
   
}
