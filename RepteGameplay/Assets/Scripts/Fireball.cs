using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fireball : MonoBehaviour
{
    public GameObject boladefuego;
    public float timespawn;
    public float repeatSpawn;


    public Transform xRangeLeft;
    public Transform xRangeright;

    public Transform yRangeup;
    public Transform yRangedown;
    public GameOver gameover;

    private new Animator animation;
    // Start is called before the first frame update
    void Start()
    {
        animation = GetComponent<Animator>();
        InvokeRepeating("SpawnFireball", timespawn, repeatSpawn);
        
    }

  

    private void OnTriggerEnter2D(Collider2D collision)
    {
        animation.SetBool("isExplosio", true);
        animation.Play("Explosion");
        if (collision.gameObject.CompareTag("Player"))
        {

            // FadeOut();
            gameover.Setup();
           // SceneManager.LoadScene("ResultScreen");      
        }
        Destroy(this.gameObject.GetComponent<Rigidbody2D>());
        
    }

    private void Explo()
    {
        Destroy(this.gameObject.GetComponent<Rigidbody2D>());
        Destroy(gameObject);
    }
    /*public void FadeOut()
    {
        animation.Play("FaceOut");
    }*/
    public void SpawnFireball()
    {
        Vector3 spawnpos = new Vector3(0, 0, 0);
        spawnpos = new Vector3(Random.Range(xRangeLeft.position.x, xRangeright.position.x), yRangeup.position.y, 0);
        GameObject fireballs = Instantiate(boladefuego, spawnpos, gameObject.transform.rotation);
    }

 
}
