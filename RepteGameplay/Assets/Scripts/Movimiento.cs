using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{
    private float defaultpeso;
    public float speed;
    public float peso;
    private float finalspeed;
    private bool mirader = true;

    private new Rigidbody2D rigidbody;
    private BoxCollider2D boxColl;
    public LayerMask suelo;
    public float fuerzadesalto;
    public GameOver gameover;

    private new Animator animation;
   
    
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boxColl = GetComponent<BoxCollider2D>();
        animation = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        MoverPersonaje();
        Saltar();
        if (transform.position.y <= -3 || transform.position.y >= 10)
        {
            gameover.Setup();
            //SceneManager.LoadScene("ResultScreen");
        }

    }


    bool EstaEnSuelo()
    {
        RaycastHit2D raycastHit= Physics2D.BoxCast(boxColl.bounds.center, new Vector2(boxColl.bounds.size.x, boxColl.bounds.size.y), 
            0f, Vector2.down, 0.2f, suelo);


         if (raycastHit.collider != null)
         {
             animation.SetBool("isJumping", true);
         } else 
         {
             animation.SetBool("isRunning", true);
         }
        return raycastHit.collider != null;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Escenario"))
        {

            // FadeOut();
            SceneManager.LoadScene("ResultScreen");
        }
        

    }
    void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnSuelo())
        {
            rigidbody.AddForce(Vector2.up * fuerzadesalto, ForceMode2D.Impulse);

        }
    }

    void MoverPersonaje()
    {
        defaultpeso = 20;
        finalspeed = ((defaultpeso * (speed) / peso));
        float inputmov = Input.GetAxis("Horizontal");

        if(inputmov != 0f)
        {
            animation.SetBool("isRunning", true);
        }
        else
        {
            animation.SetBool("isRunning", false);
        }

        rigidbody.velocity = new Vector2(inputmov* finalspeed, rigidbody.velocity.y);
        Orientacion(inputmov);
    }

    void Orientacion(float inputmov)
    {
        if ((mirader == true && inputmov < 0) || (mirader == false && inputmov > 0))
        {
            mirader = !mirader;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }
}
